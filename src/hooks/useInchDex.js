import { useEffect, useState } from "react";
import { useMoralis } from "react-moralis";
import { useMoralisDapp } from "providers/MoralisDappProvider/MoralisDappProvider";
import useVpool from "./useVPool";

const useInchDex = (chain) => {
  const { Moralis, web3 } = useMoralis();
  const { walletAddress } = useMoralisDapp();
  const [tokenList, setTokenlist] = useState();
  const {
    getTotalPoolFee,
    quote,
    costVirtuswap,
    costUniswapDirect,
    costUniswapIndirect,
    getRealPoolFee,
    getRPoolTokenABalance,
  } = useVpool();

  useEffect(() => {
    //   if (!Moralis?.["Plugins"]?.["oneInch"]) return null;
    //   Moralis.Plugins.oneInch
    //     .getSupportedTokens({ chain })
    //     .then((tokens) => setTokenlist(tokens.tokens));
    // }, [Moralis, chain]);

    let tokens = {
      BTC: {
        logoURI:
          "https://assets.coingecko.com/coins/images/1/large/bitcoin.png?1547033579",
        name: "Bitcoin",
        symbol: "BTC",
        address: "0x28aA2245b0B9c94f6E2181618f1D66166D0d2068",
      },
      ETH: {
        logoURI:
          "https://assets.coingecko.com/coins/images/279/large/ethereum.png?1595348880",
        name: "Ethereum",
        symbol: "ETH",
        address: "0x71eb04E6989f47D9f62899be5a9F235A4cA2Fe47",
      },
      USDT: {
        logoURI:
          "https://assets.coingecko.com/coins/images/325/large/Tether-logo.png?1598003707",
        name: "USDT",
        symbol: "USDT",
        address: "0xd0BFD69F674062b624261Bac479A44fD9F119C4e",
      },
      SHIB: {
        logoURI:
          "https://assets.coingecko.com/coins/images/11939/small/shiba.png?1622619446",
        name: "SHIB",
        symbol: "shib",
        address: "0xf678bdb3eee1fe4da8f8b8c9f4acce2244357fba",
      },
    };

    setTokenlist(tokens);
  }, []);

  const getQuote = async (params) => {
    // await Moralis.Plugins.oneInch.quote({
    //   chain: params.chain, // The blockchain  you want to use (eth/bsc/polygon)
    //   fromTokenAddress: params.fromToken.address, // The token you want to swap
    //   toTokenAddress: params.toToken.address, // The token you want to receive
    //   amount: Moralis.Units.Token(
    //     params.fromAmount,
    //     params.fromToken.decimals
    //   ).toString(),

    const quoteRes = await quote(
      params.fromToken.address,
      params.toToken.address,
      params.fromAmount
    );

    const tPoolFee = await getTotalPoolFee(
      params.fromToken.address,
      params.toToken.address
    );

    const rPoolFee = await getRealPoolFee(
      params.fromToken.address,
      params.toToken.address
    );

    // let btcethBalance = await getRPoolTokenABalance(0);
    // let btcusdcBalance = await getRPoolTokenABalance(1);
    // btcethBalance = btcethBalance *1;
    // btcusdcBalance = btcusdcBalance *1;
    // tradeCostUniswap = tradeCostUniswap * 100;

    // //Cost_indirect (BTC-ETH) = Cost_direct(BTC-ETH) * Pool size (BTC-USDT) / Pool size (BTC-ETH) * 2
    // let indirect = (((tradeCostUniswap * btcusdcBalance) / (btcethBalance * 2))) + 0.6;

    let tradeCostVswap = await costVirtuswap(
      params.fromToken.address,
      params.toToken.address,
      params.fromAmount
    );

    let tradeCostUniswap = await costUniswapDirect(
      params.fromToken.address,
      params.toToken.address,
      params.fromAmount
    );

    let tradeCostUniswapIndirect = await costUniswapIndirect(
      params.fromToken.address,
      params.toToken.address,
      params.fromAmount
    );

    let indirectCost = web3.utils.fromWei(tradeCostUniswapIndirect,'ether');

    tradeCostVswap = (tradeCostVswap / 10000 + tPoolFee) * 100;

    tradeCostUniswap = (tradeCostUniswap / 10000 + rPoolFee) * 100;

    indirectCost = indirectCost * 100;

    //take min
    quoteRes.tradeCostUniswap = (tradeCostUniswap).toFixed(3);
    quoteRes.tradeCostVswap = (tradeCostVswap).toFixed(3);
    quoteRes.tradeCostIndirect = (indirectCost * 1).toFixed(3);

    return quoteRes;
  };

  async function trySwap(params) {
    // const { fromToken, fromAmount, chain } = params;
    // const amount = Moralis.Units.Token(
    //   fromAmount,
    //   fromToken.decimals
    // ).toString();
    // if (fromToken.address !== "0xeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeeee") {
    //   await Moralis.Plugins.oneInch
    //     .hasAllowance({
    //       chain, // The blockchain you want to use (eth/bsc/polygon)
    //       fromTokenAddress: fromToken.address, // The token you want to swap
    //       fromAddress: walletAddress, // Your wallet address
    //       amount,
    //     })
    //     .then(async (allowance) => {
    //       console.log(allowance);
    //       if (!allowance) {
    //         await Moralis.Plugins.oneInch.approve({
    //           chain, // The blockchain you want to use (eth/bsc/polygon)
    //           tokenAddress: fromToken.address, // The token you want to swap
    //           fromAddress: walletAddress, // Your wallet address
    //         });
    //       }
    //     })
    //     .catch((e) => alert(e.message));
    // }
    // await doSwap(params)
    // .then((receipt) => {
    //   if (receipt.statusCode !== 400) {
    //     alert("Swap Complete");
    //   }
    //   console.log(receipt);
    // })
    // .catch((e) => alert(e.message));
  }

  async function doSwap(params) {
    // return await Moralis.Plugins.oneInch.swap({
    //   chain: params.chain, // The blockchain you want to use (eth/bsc/polygon)
    //   fromTokenAddress: params.fromToken.address, // The token you want to swap
    //   toTokenAddress: params.toToken.address, // The token you want to receive
    //   amount: Moralis.Units.Token(
    //     params.fromAmount,
    //     params.fromToken.decimals
    //   ).toString(),
    //   fromAddress: walletAddress, // Your wallet address
    //   slippage: 1,
    // });
  }

  return { getQuote, trySwap, tokenList };
};

export default useInchDex;
