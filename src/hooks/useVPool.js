import { FileUnknownFilled } from "@ant-design/icons";
import { useChain } from "react-moralis";
import { useMoralis } from "react-moralis";

const contractArtifact = require("../Artifacts/vPool.json");
const Web3 = require("web3");

let sendArgsAccount = {};

const useVpool = () => {
  const { chain } = useChain();
  const { web3, Moralis, user } = useMoralis();

  const vSwapAddress = "0x2A0967A8f22D1117f5Bf3119fd27A0C19a167e20"; // Mumbai

  const provider = web3?.givenProvider;

  // const web3 = new Web3(provider);

  const vPool = new web3.eth.Contract(contractArtifact.abi, vSwapAddress); //local dev

  let sendArgs = {};

  async function getTokens() {
    const poolTokens = await vPool.methods.getTokens().call();
    return poolTokens;
  }

  async function getContractAddress() {
    return vSwapAddress;
  }

  async function getAccount() {
    return sendArgs.from;
  }

  async function getAccountBalance() {
    let weiValue = await web3.eth.getBalance(sendArgs.from);
    return web3.utils.fromWei(weiValue, "ether");
  }

  async function initPools() {
    const initPoolsTx = await vPool.methods._initPools().send(sendArgs);
    return initPoolsTx;
  }

  async function exchageReserves() {
    const exchangeReservesTx = await vPool.methods
      .exchageReserves()
      .send(sendArgs);
    return exchangeReservesTx;
  }

  async function poolsCount() {
    let poolsCount = await vPool.methods.getPoolsCount().call();
    return poolsCount;
  }

  async function getRPools() {
    const rPools = await vPool.methods.getRPools().call();
    return rPools;
  }

  async function quote(tokenAAddress, tokenBAddress, amount) {
    const weiAmount = web3.utils.toWei(amount.toString(), "ether");

    const quote = await vPool.methods
      .quote(tokenAAddress, tokenBAddress, weiAmount)
      .call();

    return {
      toTokenAmount: quote,
      toToken: {
        decimals: 18,
      },
    };
  }

  async function getTotalPoolFee(tokenAAddress, tokenBAddress) {
    let quote = await vPool.methods
      .getTotalPoolFee(tokenAAddress, tokenBAddress)
      .call();

    quote = web3.utils.fromWei(quote) * 1;

    return quote;
  }

  async function getRealPoolFee(tokenAAddress, tokenBAddress) {
    let quote = await vPool.methods
      .getRealPoolFee(tokenAAddress, tokenBAddress)
      .call();

    quote = web3.utils.fromWei(quote) * 1;

    return quote;
  }

  async function costVirtuswap(tokenAAddress, tokenBAddress, amount) {
    const weiAmount = web3.utils.toWei(amount.toString(), "ether");

    const cost = await vPool.methods
      .costVirtuswap(tokenAAddress, tokenBAddress, weiAmount)
      .call();

    return cost;
  }

  async function costUniswapDirect(tokenAAddress, tokenBAddress, amount) {
    const weiAmount = web3.utils.toWei(amount.toString(), "ether");
    const cost = await vPool.methods
      .costUniswapDirect(tokenAAddress, tokenBAddress, weiAmount)
      .call();

    return cost;
  }

  async function costUniswapIndirect(tokenAAddress, tokenBAddress, amount) {
    const weiAmount = web3.utils.toWei(amount.toString(), "ether");
    const cost = await vPool.methods
      .costUniswapIndirect(tokenAAddress, tokenBAddress, weiAmount)
      .call();

    return cost;
  }

  async function getRPoolTokenABalance(poolId) {
    const tokenABalance = await vPool.methods
      .getRPoolTokenABalance(poolId)
      .call();
    return web3.utils.fromWei(tokenABalance, "ether");
  }

  async function getVPools() {
    const vPools = await vPool.methods.getVPools().call();
    return vPools;
  }

  async function getTotalsPool() {
    const tPools = await vPool.methods.getTPools().call();
    return tPools;
  }

  async function getPoolsCount() {
    const poolsCount = await vPool.methods.getPoolsCount().call();
    return poolsCount;
  }

  async function calculateReserveRatio() {
    const reserveRatioTx = await vPool.methods
      ._calculateReserveRatio()
      .send(sendArgs);
    return reserveRatioTx;
  }

  async function calculateBelowThreshold() {
    const belowThresholdTx = await vPool.methods
      ._calculateBelowThreshold()
      .send(sendArgs);
    return belowThresholdTx;
  }

  async function calculateBelowThreshold() {
    const belowThresholdTx = await vPool.methods
      ._calculateBelowThreshold()
      .send(sendArgs);
    return belowThresholdTx;
  }

  async function calculateVirtualPools() {
    const virtualPoolsTx = await vPool.methods
      ._calculateVirtualPools()
      .send(sendArgs);
    return virtualPoolsTx;
  }

  async function getPoolsInitialized() {
    const poolsInitialized = await vPool.methods.getPoolsInitialized().call();
    return poolsInitialized;
  }
  async function testNums() {
    const testNums = await vPool.methods.testNums().send(sendArgs);
    return testNums;
  }

  async function swap(inTokenAddress, outTokenAddress, amount) {
    const estimateGas = await vPool.methods
      .swap(inTokenAddress, outTokenAddress, amount)
      .estimateGas(sendArgsAccount);
    sendArgs.gas = estimateGas;
    const swapTx = await vPool.methods
      .swap(inTokenAddress, outTokenAddress, amount)
      .send(sendArgs);
    return swapTx;
  }

  async function getPoolReserves(tokenAAddress, tokenBAddress) {
    const poolReserves = await vPool.methods
      .getPoolReserve(tokenAAddress, tokenBAddress)
      .call();
    return poolReserves;
  }

  return {
    calculateVirtualPools,
    calculateBelowThreshold,
    calculateReserveRatio,
    getPoolsCount,
    getTotalsPool,
    getVPools,
    getRPools,
    poolsCount,
    exchageReserves,
    initPools,
    getAccountBalance,
    getAccount,
    getContractAddress,
    getTokens,
    getPoolReserves,
    swap,
    getPoolsInitialized,
    quote,
    getTotalPoolFee,
    getRealPoolFee,
    costVirtuswap,
    costUniswapDirect,
    getRPoolTokenABalance,
    costUniswapIndirect
  };
};

export default useVpool;
