import { useChain } from "react-moralis";
import { useMoralis } from "react-moralis";
import useVpool from "./useVPool";

const ERC20 = require("../Artifacts/ERC20.json");

const erc20ABI = [
  {
    constant: true,
    inputs: [],
    name: "name",
    outputs: [
      {
        name: "",
        type: "string",
      },
    ],
    payable: false,
    stateMutability: "view",
    type: "function",
  },
  {
    constant: false,
    inputs: [
      {
        name: "_spender",
        type: "address",
      },
      {
        name: "_value",
        type: "uint256",
      },
    ],
    name: "approve",
    outputs: [
      {
        name: "",
        type: "bool",
      },
    ],
    payable: false,
    stateMutability: "nonpayable",
    type: "function",
  },
  {
    constant: true,
    inputs: [],
    name: "totalSupply",
    outputs: [
      {
        name: "",
        type: "uint256",
      },
    ],
    payable: false,
    stateMutability: "view",
    type: "function",
  },
  {
    constant: false,
    inputs: [
      {
        name: "_from",
        type: "address",
      },
      {
        name: "_to",
        type: "address",
      },
      {
        name: "_value",
        type: "uint256",
      },
    ],
    name: "transferFrom",
    outputs: [
      {
        name: "",
        type: "bool",
      },
    ],
    payable: false,
    stateMutability: "nonpayable",
    type: "function",
  },
  {
    constant: true,
    inputs: [],
    name: "decimals",
    outputs: [
      {
        name: "",
        type: "uint8",
      },
    ],
    payable: false,
    stateMutability: "view",
    type: "function",
  },
  {
    constant: true,
    inputs: [
      {
        name: "_owner",
        type: "address",
      },
    ],
    name: "balanceOf",
    outputs: [
      {
        name: "balance",
        type: "uint256",
      },
    ],
    payable: false,
    stateMutability: "view",
    type: "function",
  },
  {
    constant: true,
    inputs: [],
    name: "symbol",
    outputs: [
      {
        name: "",
        type: "string",
      },
    ],
    payable: false,
    stateMutability: "view",
    type: "function",
  },
  {
    constant: false,
    inputs: [
      {
        name: "_to",
        type: "address",
      },
      {
        name: "_value",
        type: "uint256",
      },
    ],
    name: "transfer",
    outputs: [
      {
        name: "",
        type: "bool",
      },
    ],
    payable: false,
    stateMutability: "nonpayable",
    type: "function",
  },
  {
    constant: true,
    inputs: [
      {
        name: "_owner",
        type: "address",
      },
      {
        name: "_spender",
        type: "address",
      },
    ],
    name: "allowance",
    outputs: [
      {
        name: "",
        type: "uint256",
      },
    ],
    payable: false,
    stateMutability: "view",
    type: "function",
  },
  {
    payable: true,
    stateMutability: "payable",
    type: "fallback",
  },
  {
    anonymous: false,
    inputs: [
      {
        indexed: true,
        name: "owner",
        type: "address",
      },
      {
        indexed: true,
        name: "spender",
        type: "address",
      },
      {
        indexed: false,
        name: "value",
        type: "uint256",
      },
    ],
    name: "Approval",
    type: "event",
  },
  {
    anonymous: false,
    inputs: [
      {
        indexed: true,
        name: "from",
        type: "address",
      },
      {
        indexed: true,
        name: "to",
        type: "address",
      },
      {
        indexed: false,
        name: "value",
        type: "uint256",
      },
    ],
    name: "Transfer",
    type: "event",
  },
];

const Web3 = require("web3");

let sendArgsAccount = {};

const useERC20 = () => {
  const { chain } = useChain();
  const { web3, Moralis, user, vSwapAddress } = useMoralis();
  const { getContractAddress } = useVpool();

  const provider = web3?.givenProvider;

  const balanceOf = async function (contractAddress, address) {
    const ERC20Instance = new web3.eth.Contract(erc20ABI, contractAddress); //local dev

    const balanceTx = await this.ERC20Instance.methods.balanceOf(address);
    return await balanceTx.call();
  };

  const allowance = async function (contractAddress, address) {
    const ERC20Instance = new web3.eth.Contract(erc20ABI, contractAddress); //local dev

    const vswapAddress = await getContractAddress();
    const balanceTx = await ERC20Instance.methods.allowance(
      address,
      vswapAddress
    );
    return await balanceTx.call();
  };

  const approve = async function (contractAddress, spenderAddress, address) {
    const ERC20Instance = new web3.eth.Contract(erc20ABI, contractAddress); //local dev

    const amountToApprove = web3.utils.toWei("10000", "ether");
    // const estimateGas = await ERC20Instance.methods
    //   .approve(address,amountToApprove  )
    //   .estimateGas(sendArgsAccount);

    // let args = { gas: estimateGas };

    const approveTx = await ERC20Instance.methods.approve(
      address,
      amountToApprove
    );

    return await approveTx.send();
  };

  return { balanceOf, allowance, approve };
};

export default useERC20;
