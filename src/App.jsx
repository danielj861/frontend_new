import { useEffect } from 'react';
import { useMoralis } from 'react-moralis';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect,
} from 'react-router-dom';
import Account from 'components/Account';
import Chains from 'components/Chains';
import TokenPrice from 'components/TokenPrice';
import ERC20Balance from 'components/ERC20Balance';
import ERC20Transfers from 'components/ERC20Transfers';
import InchDex from 'components/InchDex';
import NFTBalance from 'components/NFTBalance';
import Wallet from 'components/Wallet';
import { Layout, Tabs } from 'antd';
import 'antd/dist/antd.css';
import NativeBalance from 'components/NativeBalance';
import './style.css';
import QuickStart from 'components/QuickStart';
import Contract from 'components/Contract/Contract';
import Text from 'antd/lib/typography/Text';
import Ramper from 'components/Ramper';
import MenuItems from './components/MenuItems';
import { useChain } from 'react-moralis';
import { networkConfigs } from 'helpers/networks';
import MetaMaskPage from '../src/components/metaMaskPage/metaMaskPage';
import StatusPool from 'components/statusePool/statusPool';
const { Header, Footer } = Layout;

const styles = {
  content: {
    display: 'flex',
    justifyContent: 'center',
    fontFamily: 'Roboto, sans-serif',
    color: '#041836',
    marginTop: '130px',
    padding: '10px',
  },
  header: {
    position: 'fixed',
    zIndex: 1,
    width: '100%',
    background: '#fff',
    display: 'flex',
    justifyContent: 'space-between',
    alignItems: 'center',
    fontFamily: 'Roboto, sans-serif',
    borderBottom: '2px solid rgba(0, 0, 0, 0.06)',
    padding: '0 10px',
    boxShadow: '0 1px 10px rgb(151 164 175 / 10%)',
  },
  headerRight: {
    display: 'flex',
    gap: '20px',
    alignItems: 'center',
    fontSize: '15px',
    fontWeight: '600',
  },
};
const App = ({ isServerInfo }) => {
  const { isWeb3Enabled, enableWeb3, isAuthenticated, isWeb3EnableLoading } =
    useMoralis();
  const { chainId } = useChain();

  useEffect(() => {
    if (isAuthenticated && !isWeb3Enabled && !isWeb3EnableLoading) enableWeb3();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [isAuthenticated, isWeb3Enabled]);

  return (
    <>
      {!window.ethereum || window.ethereum.chainId !== '0x13881' ? (
        <MetaMaskPage />
      ) : (
        <Layout style={{ height: '100vh', overflow: 'auto' }}>
          <Router>
            <Header style={styles.header}>
              <Logo />
              <MenuItems />
              <div style={styles.headerRight}>
                <Chains />
                <NativeBalance />
                <Account />
              </div>
            </Header>
            <div style={styles.content}>
              {!isAuthenticated ? (
                <>Please login using the "Authenticate" button</>
              ) : (
                <Switch>
                  <Route path="/trade">
                    <InchDex chain="polygon" />
                  </Route>
                  <Route path="/liquidity">
                    <ERC20Balance />
                  </Route>
                  <Route path="/status">
                    <StatusPool />
                  </Route>
                </Switch>
              )}
            </div>
          </Router>
          <Footer style={{ textAlign: 'center' }}>
            <Text>Virtuswap 2022</Text>
          </Footer>
        </Layout>
      )}
    </>
  );
};

export const Logo = () => (
  <div style={{ display: 'flex' }}>
    <img
      src={`data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAJ0AAAA2CAIAAABSnwysAAAAAXNSR0IArs4c6QAAAARnQU1BAACxjwv8YQUAAAAJcEhZcwAADsMAAA7DAcdvqGQAABC/SURBVHhe7VsHeFPXFbaNbcwyxgybDTF7W/KUlyxbsiXLljUMZe89QiizJUChgSZA+KAByghhJAQIrpOyCTODEAihadhmpIYUwggQyga7/3vn6vL8JIETkxLL/r/76Xu659z75Pu/859zr2QPRRncEWW8uifKeHVPlPHqnijj1T1Rxqt7ooxX90QZr3IoFYpIZWiUMpS9L5koXbyCM11425FxIYuTgzemBu4yBGzTV3tXV2uKuqElqlW4QuBycEzTb9Mrfm2sPDCmKY0qiSgtvEYoQvupmm3RB96weBdkeTi2+zavb9IrTUpoNEtT747N66rFZ66mLhtcAlEqeDVHtlqvq3HLUi7f5vH09tDmedfq9cDqedvqtUIbxMaXQLg5rxDeIbFNDqdX4nEp8IcL8VV2LVjtnXj9MKV6eInNss+B1+jo6MTExLCwMPb+NwOQ+kpcyJGMigJ5RKq9Ob2WdqIh9SaFt2NzlTQUl9cxY8ZcvHjx8ePHBw8ezMjIUCqxmD8DaWlpQ4cOHS1i0KBBeD6Y4Xmgt6rZF2n+QvwhFsWGC8gsaqI5mroDVE07RrXsrmo+KaHhttRqP5q9yYG3Tw3+1shWbK6ShmLxqlar8/LyQGq+iGXLlqGH2YqGKVOmnD9/nobn5ub26tWLGYoNhNoHuhqPbJ4CSRSLNo/Dxkovx4VgG8OcROgj2rymbnDAWOWe1VOIVNEfr18ZK/dRNWNOJQ3F4tVsNl++fLnAjs2bN2u1WmYrGsDrhQsXaPipU6eeI6+TExrmZZYHndQeZ3lu1gdaHOIPGXS2ph7KJe7JG5juHt2C+ZU0FIvX8PDw/fv3P3jwgIiZNGmSSqVitqJh8uTJUl579uzJDMWDNrwtqh6uvXjdbQjoHOWEJKSNiQmNLpl9H9s8UDDnZlT41FB1XUpN7Ha6Rbf4eUnlt4Ti5tfk5ORFixYhUsePH/9zSQUoXkmHT548+bx4HR4b8i/UwHZFReC+mtDIFUlhitCEsPaa8HZ0LuEeKC6vxYSU1+PHj/fo0YMZiofZifWuSeogxK4xog2zlQ484TVHgnnz5kFjmUEEqtZ169Yxc07O8OHDIyMjMzMzV61axbpyckaOHEkhi7J26dKlrDcnZ/DgwQkJCQMGDFiwYEF2djYuUAbPnz8fpkOHDt2+fZt0+MaNG3v27EHn7NmzDQYD5unYsePChQvFOQSMGjUqNjZW/DgKRPbixYuZISdnyJAh/ANHKELf1dYS5FdMkyiAUf3yYE0Ob4u3xzIqHs2omJNSfUJ8Y114W2ZzBkRzv5hmf0+pkZMqtPd0tUbFvcRsCoUxovWsxHqLk4LRpqvrowSj/mhlhwExTWkI2jvaoO7RzdEPSUiLaDM1ocFHqdW/Ta900ex7zeKN12/SK63UBnVxSOdxYe3f19WkSdboalqiWg2NbbIxNRAfHvu3TamBr8SGhDkToie8Xr16leIGuHfvHgSWGUTs3bsXeZSsKIC7deuGdezTp8+VK1eoE4Aggz84p6SknDt3jvXm52/duhW5kw+fMWNG165djx49Sm8dsW/fvs6dO2MePD0IYtabn79kyRL+qV599dWzZ88yQ37+nDlz8JyRCZUwCOOV7b8zy/8hoRGZQBJCmR082R1+MPus1NbCkrkS6hGxIVBywT9LOJBC/RUhVtTYsIPje1Ry24Qb/TGe3QgEr9XV5Hc5aaqQGdka/b+LannTWo7dmqyS61vWckuSg6VbRRQKP+HT2m99IqOCbMjjLI/P0/zVYe3ZADue8Lpy5Uq+YwEmTpzIIyApKen69evMkJ9/4MAB2mi64hVVsXTRZfi1eU0Nb7shJZD95TaPUxkVfm+PMCTR98RQdmzfmcr/Wd0AcUaeUmCb+7GhGl9Q1MnWKKGuTgxvN09Th89w3ez9TjI7egR/CEfqv2f12mEIILLQ/6PFmw9xbNct3n9KaCj6CmC8wiTeHfIjdaaGiu/LtCqy4uAJr506dQKv0EMsE1537NjBVwpV6507d6gfmDZtWnR0NPrBK0U59XNe8RwgXqkT4A63bt06duwY5BQCvn79+oMHD+KxoJsCEIkTJ06gc/ny5SaTCfOAV/SQFZDyitqbbkGTS3mF0n6YKsQr6fA5k9+4+MZkgkSDibs2LzJxH7pGJK3QBqnD5GdMqKr+qqnDPU+b/MbGCROaI1tBCfkkCJ1P06pic6xUhA6ObYI4pv4fMn3+oq5PU+ER+cHig06Mum31+i6z/LlMvweSXRY23NBYThL+FgQxt6Jh4CWLD8byt3gFtZPsmkQoVDdh0XnIgkiNRkP94Pjhw4fUjyyI5EfnSq7iFdF85swZ9NC6A9jmIkwxoexAasOGDXfv3iWfvLy8vn37MoMIileBUhGOvNJAQMqrStkBOUn4g/E4Z3ncsHi/nlif39UQ0ebt5OArZh+shfC8291wjQXCIiLPxReWNYxFxCMRCktp88DF4uTa6O+janaShJE2VFke32ZUyopqCVV8U1OPzwzBAJ00VUZka2jJjMT62EkTeUgNnaJawodIwlR4Dvg5F+JVyBoieWiIXSg/EiryLlK+4C/eBX8Lbk1DCIV4nTlz5qNHj2ilsI7jxo2DFKvVaogw3lI/4iwuLo78XfEaHx8v5RXDUXbxdZdi48aNnFfwhAmZQcRTeIUOu+IVeEtTB8shLKvYVmtrQYGZTQQCFwnvzcR6x9Mrkg8a+V82+6Cwkh12d41u8VmaP/H6yOaxXV8Nig2yhSMqyVhKsabI1lv11ajzvtVzt6FqoWdZgihlKCT9NXWDr42V+ae9avYeZP/ql+swbo2w/kdKdeoHYpQdcDu6Cw1kBhGF3iAv3r9/n61iQcGWLVuioqKwMcXSs66Cgv79+/O8SzrMDBIdBq9Ifqy3oGDTpk16vZ6GyIBbQH7JDUN69+7NDCKKosMEGa/j4hrnihFADcr2cmwIsxUGhG5+Uh3kPO6M9s/0SrIv1eG2NDmYOxwyVu4f0/StJCbOEE8kUVxAG5YnC6Vvnkk86rJ5XLH4vFn4e1yoNB6yyfENdxkCZPelhs5hsU3IWarD4PUjCa8I95W6WmyUSDwziCj0Bti9ezeXYkgugnXz5s28lIVQc3EGnhmvBASlK163bdsGXsnt9OnTsnPEX1Y3AebI1tvEiCFlg0xl62rwTYgMUcoO09QNLpl9SNbgj9SITYW0hkLAIUkjq9GcZ01+KFy3iMn1jtXrSHrFzwz+uH6U5fmFscr4uMb81qdMFRDrbBaFAhKNfdFVXjrZ7yi9xs6b8yqth+/bPLERp34gXBmK/CIdzgwi5LyOHj2ap1JgwoQJYJe9yc/HzpIqJkIReX1KvG7fvh0KQW6O502/mFfQMFsjHE2wv9kmJE6kVVSwzKMwkK7maepip8uX6bCxMjagzCyil6oZ1JKsKH0RskiKuL5g8l2YVHtxUm0yXcz0hfAK14gwq+cnhqrIoDRDcni7ZdrgJ0yIkg6O9xqqCo+FvdMJr+JsjrziEeGj0JhBhJxXCO+1a9eYuhUU0HdwdA01NhqN0sKHdBjLSg6udBhiS+cMjti5cyceI3KD5MrOm4qiw3R3Ga8AilVkQagTVoTECrRhXdDvNNt1jmqx0xDA/a+a5b+DQcG12q57KH2xm6RvC75Jr9QjujlyLdSYhqOfTWLxQflNwyGb0Pb79lIcM2BPDA1A/YynCveiftBTRB1GifBBilgeoonEM4MIOa/AmjVraK3FSBAgLKpIDz/uITwzXmkSKLkrXiH7XB5AoSxeUW1B+enuQHZ2dkZGBpmmTp2K+pkGAo68AmPiXhLqVfFvZquW5XEh03e5Nqi7qnlMWKGtKtZ3lqYe1o788fpRanXprxIjlKFTEhoSZ+SA1cTbj/UBGIucepy+wLcPx+vpDL/eKuGYCUgIa4dyjH8YBCWijUwov6EW9PHgIItXXg/js0njFZKOql4wYU4x9JlBhBNeETRUPbE1s/M6YsSIiIgI5iSiiLxu3brVFa+7du3ivKIU37Nnz9q1a1evXt2xY0dYu3fvfvjwYbo7cPPmzRUrVoBssIj50UMDAae8gonX1fUvZfoSB/T3U8P1fy3lsPuUfskzMjbkTIYfd9itD5CdKvdXNWUPit0HxTO2tjDBEymcOsn60Oq5z+DPv+tFrQS55gORqlEBYD/TK7r5u9paEFs+0KkOk/X7TF9snZFNOke3xOTkDxMKiIPGKjSE4ITXsLCw3NxcLBatJuH8+fOOP2YgHWYernUYSTQtLY2GyIAQ5OfDHEeOHKHAxYYKW2eeCJ4Cp7wCqImmJ9b/3uxL8SpreNizU2rwgwjsR7E63PqF0V/21R42MDkp1bkDGmgeEiNwgF3HVHUDqQnb3L+J21xCrLI99jNSB1fNlQ4/pUHVR8U+ObUGnPAKzJ07l9fABHAmrZgIRaybUPS6iteJEydK5ZRw9OhRXhjLzh9cwRWvAMqW4bFNUAcJKmdXSP6KzYZNPBQEkCY/pyAQTV8aq3QrfBCvUnaYoRZLUJrBJpzN8loMdRbClw8/bfLrH1Po5xZ4Sv5j9pUOd3otjVfwyuthNFDI3fgQBOt2fYDs9N85ryhf6eCQAKm0WCyOv0wrIq9PiVeQgVHSrxwA1MB8I4u98vTp02Xco5rD3klaKj+FVwIEDfkM+xN2zCQ2ZKz3dTVRtpAPaECVy63gjJ8TcSA6sTclh5uWciu1tZhBoYCo7taLlbCY7b5MqyI7bcZbSD0KZvKhho3v/rQqMyWVrfN6WBRb5I5HVuZGDTfalBKIB478OZzzCvTr12+YHX379uVnEVJgdztw4EDmNGyYyWQiN6RhDGG9w4Z16dLFMdY5UGCjzB47duysWbPeeOMNbLRQHMkSuU6nQ/+CBQsWLlw4ZswYvI2JicG0Q+yQFequgAplcEzTVdqgr4yVUeaggIK0Mpu4Q6V4InFDQeT4FR5m6K5qgXVHw3OAIpkZRM23RLYiE+6S5fBMAHQoMSG+8dva4BXaICgz1ILOBWkgGspm/jtIqQ6jRsOOCI/aouTaKPXx8TAD0jN5yuCS19IGrKxQ15DEiVvPtbqaL/ynswKv9noYvCJrMMOzUCp4BT3YFSAIeHXqCGRHIQfb9e2Cqfx0dQNme3GQ1sMosEs7r9BDSNyalJoQrlOmCj9Zy6FixNYCEQm1ZE4SQE6Fr8HtCoy2L82/kzMh/T9DpsO7SzOv+og2G8WTW1JUeqWlWaOr6fjTAl1E23e0QewQUfS/YSm3KKn2s9P1rw9pPYwqr1THa3pkmy2p4qG/vaFoPJpecZ6mLt/SEMKVoT1VzXfqA5BNpf6IcsdfGr8QoEzbkFodJRLaNn21OUX+Fz835BVr8UFKDQpQNArWOzYv7Eex1UEe7Rzdsl9MM+xEPzFU5SpHbmgnTRX47ytKLtyQV1RJy5ODhFN4BJ+oq5yzJ2+lJvs1HC6YfcH3Cy+Diw/3rJvmaerclJy4CpzZj1j5W8frcya/yQkNZT8AK6FwT15fU9e/KJ4JQ37RKFif0u7bvPYYqiLX/hZqpecC9+R1XFxj+tJ7hTbolbgQlJHCz50kOsxf/2v1+jzNHz6OR3ElGu7JK6rZranVzmeWh64iWSIKUyPajo57aWly8IbUwB36gE36wGXa4PHxjY2RbdwgmzrCPXkFsIeJEH7KW0rhtryWcpTx6p4o49U9Ucare6KMV/dEGa/uiTJe3REKxf8A9BjC2Ms9YrIAAAAASUVORK5CYII=`}
    />
  </div>
);

export default App;
