import React from 'react';
import Web3 from 'web3';
import { useEffect } from 'react';
import { useWeb3React } from '@web3-react/core';
import { useChain } from 'react-moralis';
import useERC20 from 'hooks/useERC20';
import { useMoralisDapp } from 'providers/MoralisDappProvider/MoralisDappProvider';

function InchModal({
  open,
  onClose,
  setToken,
  tokenList,
  setTokenBalance,
  setFromTokenAllowanceAmount,
  fromTokenAllowanceAmount,
}) {
  const { walletAddress, chainId } = useMoralisDapp();
  const { chain } = useChain();
  const erc20Instance = useERC20();

  useEffect(() => {
    setFromTokenAllowanceAmount(fromTokenAllowanceAmount);
  }, [fromTokenAllowanceAmount]);

  if (!open) return null;

  const minABI = [
    {
      constant: true,
      inputs: [{ name: '_owner', type: 'address' }],
      name: 'balanceOf',
      outputs: [{ name: 'balance', type: 'uint256' }],
      type: 'function',
    },
  ];

  const provider = chain.rpc[0];
  const web3Client = new Web3(new Web3.providers.HttpProvider(provider));

  const getBalance = async (tokenAddress) => {
    const contract = new web3Client.eth.Contract(minABI, tokenAddress);
    const result = await contract.methods.balanceOf(walletAddress).call();
    console.log('res:' + walletAddress);
    const format = web3Client.utils.fromWei(result);
    setTokenBalance(format);
  };

  return (
    <div style={{ overflow: 'auto', height: '500px' }}>
      {!tokenList
        ? null
        : Object.keys(tokenList).map((token, index) => (
            <div
              style={{
                padding: '5px 20px',
                display: 'flex',
                alignItems: 'center',
                cursor: 'pointer',
              }}
              onClick={async () => {
                setToken(tokenList[token]);
                getBalance(tokenList[token].address);

                if (fromTokenAllowanceAmount) {
                  const allowanceAmount = await erc20Instance.allowance(
                    tokenList[token].address,
                    walletAddress
                  );
                  fromTokenAllowanceAmount.allowance = allowanceAmount;
                }
                onClose();
              }}
              key={index}
            >
              <img
                style={{
                  height: '32px',
                  width: '32px',
                  marginRight: '20px',
                }}
                src={tokenList[token].logoURI}
                alt="noLogo"
              />
              <div>
                <h4>{tokenList[token].name}</h4>
                <span
                  style={{
                    fontWeight: '600',
                    fontSize: '15px',
                    lineHeight: '14px',
                  }}
                >
                  {tokenList[token].symbol}
                </span>
              </div>
            </div>
          ))}
    </div>
  );
}

export default InchModal;
