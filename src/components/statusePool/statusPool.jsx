import React, { useState } from 'react';
import { Table, Skeleton } from 'antd';
import useVpool from 'hooks/useVPool';
import { useMoralis } from 'react-moralis';
import { useEffect } from 'react/cjs/react.development';
function StatusPool() {
  const [rPools, setRPools] = useState([]);
  const [vPools, setVpools] = useState([]);
  const [totalsPool, setTotalsPool] = useState([]);

  class VPoolVM {
    constructor() {
      this.tokenAName = '';
      this.tokenBName = '';
      this.fee = 0;
      this.tokenABalance = 0;
      this.tokenBBalance = 0;
    }
  }

  class RPoolVM {
    constructor() {
      this.tokenA = {};
      this.tokenB = {};
      this.fee = 0;
      this.reserveRatio = 0;
      this.belowReserve = 1;
      this.tokenABalance = 0;
      this.tokenBBalance = 0;
      this.maxReserveRatio = 0;
    }
  }

  const { web3 } = useMoralis();
  const Web3 = web3;

  function toEtherAdjusted(num, fixNum) {
    if (!num) return;
    if (fixNum == undefined) fixNum = 2;
    return parseFloat(Web3.utils.fromWei(num.toString(), 'ether')).toFixed(
      fixNum
    );
  }

  function parseRPoolVM(rPools) {
    let arr = new Array();
    for (let i = 0; i < rPools.length; i++) {
      let vm = new RPoolVM();
      vm.tokenA.name = rPools[i].tokenA.name;
      vm.tokenA.price = toEtherAdjusted(rPools[i].tokenA.price);
      vm.tokenA.tokenAddress = rPools[i].tokenA.tokenAddress;

      vm.tokenB.name = rPools[i].tokenB.name;
      vm.tokenB.price = toEtherAdjusted(rPools[i].tokenB.price);
      vm.tokenB.tokenAddress = rPools[i].tokenB.tokenAddress;
      vm.fee = toEtherAdjusted(rPools[i].fee, 4) * 100;

      vm.reserveRatio = rPools[i].reserveRatio;
      vm.belowReserve = rPools[i].belowReserve;
      vm.tokenABalance = toEtherAdjusted(rPools[i].tokenABalance);
      vm.tokenBBalance = toEtherAdjusted(rPools[i].tokenBBalance);
      vm.maxReserveRatio = toEtherAdjusted(rPools[i].maxReserveRatio);
      arr.push(vm);
    }

    return arr;
  }

  function parseVPoolVM(vPools) {
    let arr = new Array();
    for (let i = 0; i < vPools.length; i++) {
      let vm = new VPoolVM();

      vm.tokenAName = vPools[i].tokenAName;
      vm.tokenBName = vPools[i].tokenBName;
      vm.tokenABalance = toEtherAdjusted(vPools[i].tokenABalance);
      vm.tokenBBalance = toEtherAdjusted(vPools[i].tokenBBalance);
      vm.fee = toEtherAdjusted(vPools[i].fee, 4) * 100;

      arr.push(vm);
    }

    return arr;
  }

  const { getRPools, getVPools, getTotalsPool } = useVpool();

  const dataTable = async () => {
    try {
      getRPools()
        .then((res) => {
          let RpoolVm = parseRPoolVM(res);
          setRPools(RpoolVm);
        })
        .catch((err) => {
          console.log(err);
        });
      getVPools().then((res) => {
        let VpoolVm = parseVPoolVM(res);
        setVpools(VpoolVm);
      });
      getTotalsPool().then((res) => {
        let TotalpoolVm = parseVPoolVM(res);
        setTotalsPool(TotalpoolVm);
      });
    } catch (err) {
      console.log(err);
    }
  };
  useEffect(() => {
    try {
      let getProvider = Web3.givenProvider;
      web3.setProvider(getProvider);
      dataTable().then(() => {
        console.log(rPools);
      });
    } catch (err) {
      console.log(err);
    }
  }, []);

  const Real = [
    {
      title: 'id',
      dataIndex: 'id',
    },
    {
      title: 'Pair',
      dataIndex: 'Pair',
    },
    {
      title: 'Balances',
      dataIndex: 'Balances',
    },
    {
      title: 'bReserve',
      dataIndex: 'bReserve',
    },
    {
      title: 'fee',
      dataIndex: 'fee',
    },
    {
      title: 'reserveRatio',
      dataIndex: 'reserveRatio',
    },
    {
      title: 'm revers',
      dataIndex: 'mrevers',
    },
  ];
  const Virtual = [
    {
      title: 'id',
      dataIndex: 'id',
    },
    {
      title: 'Pair',
      dataIndex: 'Pair',
    },
    {
      title: 'Balances',
      dataIndex: 'Balances',
    },

    {
      title: 'fee',
      dataIndex: 'fee',
    },
  ];
  const Total = [
    {
      title: 'id',
      dataIndex: 'id',
    },
    {
      title: 'Pair',
      dataIndex: 'Pair',
    },
    {
      title: 'Balances',
      dataIndex: 'Balances',
    },
    {
      title: 'fee',
      dataIndex: 'fee',
    },
  ];
  let counterId = 0;
  const dataReal = rPools.map((data) => {
    return {
      key: data.length,
      id: counterId++,
      Pair: `${data.tokenA.name} / ${data.tokenB.name}`,
      fee: `${data.fee}%`,
      Balances: `${data.tokenABalance} / ${data.tokenBBalance}`,
      bReserve: data.belowReserve,
      reserveRatio: `${data.reserveRatio}%`,
      mrevers: `${data.maxReserveRatio}%`,
    };
  });
  let counterVirtual = 0;
  const dataVirtual = vPools.map((data) => {
    return {
      id: counterVirtual++,
      fee: `${data.fee}%`,
      Balances: `${data.tokenABalance} / ${data.tokenBBalance}`,
      Pair: `${data.tokenAName} / ${data.tokenBName}`,
    };
  }, []);
  let counterTotal = 0;

  const dataTotal = totalsPool.map((data) => {
    return {
      id: counterTotal++,
      fee: `${data.fee}%`,
      Balances: `${data.tokenABalance} / ${data.tokenBBalance}`,
      Pair: `${data.tokenAName} / ${data.tokenBName}`,
    };
  }, []);

  return (
    <>
      <div style={{ width: '80%' }} className="containerTable">
        <Skeleton loading={!rPools}>
          {dataReal.length > 0 ? (
            <div>
              <h2 style={{ textAlign: 'center' }}>Real pools</h2>
              <Table
                  pagination={false}
                columns={Real}
                dataSource={dataReal}
                size="small"
              />
            </div>
          ) : (
            ''
          )}

          <div>
            <h2 style={{ textAlign: 'center' }}>Virtual pools</h2>
            <Table
                pagination={false}
              columns={Virtual}
              dataSource={dataVirtual}
              size="small"
            />
          </div>

          <div>
            <h2 style={{ textAlign: 'center' }}>Total pools</h2>
            <Table
                pagination={false}
              columns={Total}
              dataSource={dataTotal}
              size="small"
            />
          </div>
        </Skeleton>
      </div>
    </>
  );
}
export default StatusPool;
