import { useLocation } from "react-router";
import { Menu } from "antd";
import { NavLink } from "react-router-dom";

function MenuItems() {
  const { pathname } = useLocation();

  return (
    <Menu
      theme="light"
      mode="horizontal"
      style={{
        display: "flex",
        fontSize: "17px",
        fontWeight: "500",
        width: "100%",
        justifyContent: "center",
      }}
      defaultSelectedKeys={[pathname]}
    >
      <Menu.Item key="/trade">
        <NavLink to="/trade">🏦 AMM</NavLink>
      </Menu.Item>
      <Menu.Item key="/liquidity">
        <NavLink to="/liquidity">💰 Liquidity</NavLink>
      </Menu.Item>
      <Menu.Item key="/status">
        <NavLink to="/status"> 📋 Status</NavLink>
      </Menu.Item>
    
    </Menu>
  );
}

export default MenuItems;
